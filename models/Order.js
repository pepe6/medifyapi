var mongoose = require ('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    userId: {
        type: String
    },
    userPhone: {
        type: String
    },
    name:{
        type: String
    },
    zip:{
        type: Number
    },
    pharmacyName:{
        type: String
    },
    slackUrl:{
        type: String
    },
    products:[{
            product: String,
            quantity: Number,
            cost:Number,
            comment:String
            }],
    answered: {
        required: true,
        type: Boolean,
        default: false
    }

});

module.exports = mongoose.model("Order", schema);
