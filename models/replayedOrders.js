var mongoose = require ('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    userId: {
        type: String
    },
    userPhone: {
        type: String
    },
    name:{
        type: String
    },
    pharmacyName:{
        type: String
    },
    slackUrl:{
        type: String
    },
    products:[{
            product: String,
            quantity: Number,
            price:String, 
            comment:String
            }]
});

module.exports = mongoose.model("ReplayedOrders", schema);
