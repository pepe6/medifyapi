var Airtable = require('airtable');
var base = new Airtable({apiKey: 'keyD8xd84dyZZTNN2'}).base('appeNWJzvwBCztPUR');


airtableControllers = {
    sendToAirtableLogs(type = "sin tipo", name = "", phone = "", zip = "", products = {}){

        base('Logs').create([
            {
              "fields": {
                "type": type,
                "name":  name,
                "phone": ""+phone,
                "zip": ""+zip,
                "products": JSON.stringify(products)
              }
            },
          ], function(err, records) {
            if (err) {
              console.error(err);
              return;
            }
            records.forEach(function (record) {
              console.log(record.getId());
            });
          });
    
    },

    getPharmacyDataFromPharmacyID(id){
      return new Promise((resolve, reject) => {
  
          base('Farmacias').find(id, function(err, recordFarmacia) {
  
            if (err) { 
              console.error(err);
              reject(err);
               
            }
            resolve(
              {
                slackUrl:recordFarmacia.get('slack_url'),
                pharmacyName : recordFarmacia.get('nombre')
              }
            )
       
            
        });
  
  
      

      })
      

    },


    getFarmaciesIDFromZip (zip){

      return new Promise((resolve, reject) => {

        base('Zip').select({
          view: "Grid view"
      }).eachPage(function page(records, fetchNextPage) {
          // This function (`page`) will get called for each page of records.
      
          records.forEach(function(record) {
  
              if(record.get('zip') === '' + zip){
                    resolve(record.get('farmacias'));
                 
              }
              
          });
          fetchNextPage();
      
      }, function done(err) {
          
          if (err) { 
            console.error(err); 
            reject(err)
          
          }
         
      });
        
      })
      

    }

}

module.exports = airtableControllers
