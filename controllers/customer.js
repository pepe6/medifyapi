
var Order = require('../models/Order');
var ReplayedOrder = require('../models/replayedOrders')
const fetch = require('node-fetch');

var express = require('express');
var app = express();
var http = require('http').createServer(app);
const io = require('socket.io')(http);

const accountSid = 'ACa3f7f05c44745c1fbf5c5d58a2eb5d84';
const authToken = 'c19a73c870bf86a591dc57a50105f4d4';
const client = require('twilio')(accountSid, authToken);

const { sendToAirtableLogs, getFarmaciesIDFromZip, getPharmacyDataFromPharmacyID } = require('./airtable');

const SLACK_URL_DASHBOARD = 'https://hooks.slack.com/services/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'



io.on('connection', (socket) => {
  console.log('a user connected' + socket);
});


http.listen(3002, () => {
  console.log('listening on *:3002');
});





let priceProductId = [process.env.PRICE_PRODUCT_1,process.env.PRICE_PRODUCT_2,process.env.PRICE_PRODUCT_3,process.env.PRICE_PRODUCT_4,process.env.PRICE_PRODUCT_5]


function  sendResponseToFB(){
    new Promise(async function(resolve, reject){
        try{
            products.map((product, index) =>{
                let price = parseFloat(product.price);
                if(product.price){
                    let bodyToSend = 
                    {
                        "subscriber_id": userId,
                        "field_id":priceProductId[index],
                        "field_value":  price
                      }
                
                fetch(process.env.MANYCHAT_API + '/fb/subscriber/setCustomField'
                , {
                    method: 'post',
                    body:    JSON.stringify(bodyToSend),
                    headers: { 'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                'Authorization':'Bearer ' + process.env.MANYCHAT_BEABER
                },
                }).then(res => res.json())
                .then(json => console.log(json));
                    
                }
        
            })
        
            let result = await Order.findById(_id)
            result.answered = true;
            let savedResult = await result.save();
        
            bodyToSend={
            
                    "subscriber_id":userId,
                    "flow_ns": process.env.MANYCHAT_FLOW_TO_PURCHASE
                
            }
        
            console.log(bodyToSend)
            fetch(process.env.MANYCHAT_API + '/fb/sending/sendFlow'
                , {
                    method: 'post',
                    body:    JSON.stringify(bodyToSend),
                    headers: { 'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                'Authorization':'Bearer ' + process.env.MANYCHAT_BEABER
                },
                }).then(res => res.json())
                .then(json => console.log(json));

        }catch(err){
            reject(err)
        }
            
    })
}

async function sendToTwilo(products, _id){
   try{
        let result = await Order.findById(_id)
       
        let phone = result.userPhone;
        let slackUrl =  result.slackUrl
        let pharmacyName = result.pharmacyName

        let orderAnsweredToSave = {
            name: result.name,
            userPhone: phone,
            products:products,
            slackUrl: slackUrl,
            pharmacyName: pharmacyName
        }


        let newUserSaved = new ReplayedOrder(orderAnsweredToSave);
        let resultFromSaving = await newUserSaved.save()
        
        let productToSend = []
        productToSend.push('\n')
        let finalPrice = 0
        products.map(product =>{
            productToSend.push( product.quantity + ' - ' + product.product + ': ')
            if(product.price === undefined || parseFloat(product.price) === 0 ){
                productToSend.push('\n')
                productToSend.push('No hay en existencia')
                productToSend.push('\n')

            }else{
                productToSend.push('\n')
                productToSend.push('$' + (product.quantity * parseFloat(product.price)))
                productToSend.push('\n')
                finalPrice += (product.quantity * parseFloat(product.price))
            }
           
        })

        productToSend.push('Precio Total: ')
        productToSend.push('\n')
        productToSend.push('$ ' + finalPrice)
        productToSend.push('\n')
        productToSend.push('Hacer Pedido: ' + '\n' + process.env.FRONT_URL + '/buy?order=' + resultFromSaving._id )
      
        client.messages
        .create({
           body: 'Medify: Encontramos esta cotización: ' + productToSend.join(' '),
           from: '+13016781467',
           to: '+521'+ phone
          
         })
        .then(message => console.log('exito' + message.sid))
        .catch(err => console.log(err));
        

        result.answered = true;
        let finalResult = result.save()
        return true 
    }catch(err){
        console.log(err)
    }
    

}


function sendToSlack(text, slackUrl){
    console.log('text sending to slack', text)
    fetch(slackUrl, { method: 'POST', body: JSON.stringify({text:text}) })
    

}

module.exports = {
    saveCustomerProductsAndSendToPharmacy: async (req, res) =>{   
        let orderToSave = {
            userPhone : req.body.userPhone || '',
            userId: req.body.userId || '',
            name: req.body.name || '',
            products: req.body.products,
            zip: req.body.zip || 0
        }

        sendToAirtableLogs('Send To pharmacy', orderToSave.name, orderToSave.userPhone, orderToSave.zip, orderToSave.products)
        sendToSlack('Cliente : ' + orderToSave.name + '\n' + 'whatsapp : https://api.whatsapp.com/send?phone=+521' + orderToSave.userPhone + '\n \n' + 'productos: ' + orderToSave.products.join(''), SLACK_URL_DASHBOARD)
       
       
        console.log(orderToSave)
        try{
           
            //sendToSlack('Nueva cotización, responde en:  \n ' + req.body.zip + process.env.FRONT_URL + '/pharmacy',SLACK_URL_FARMACIA_BOSQUES)
            
            let pharmaciesID = await getFarmaciesIDFromZip(orderToSave.zip)

            pharmaciesID.map(async id => {

                let pharmacyData = await getPharmacyDataFromPharmacyID(id)

                orderToSave.pharmacyName = pharmacyData.pharmacyName;
                orderToSave.slackUrl = pharmacyData.slackUrl;

                let newUserSaved = new Order(orderToSave);

                var result = await newUserSaved.save();

                
                let textToSendToSlack = "Nueva solicitud de cotización: \n" + process.env.FRONT_URL + "/pharmacy/?order=" + result._id
                sendToSlack(textToSendToSlack, pharmacyData.slackUrl)
            })
    
            res.status(200).send({message:'ok'});
            //TO DO res.status(200).send(result);
        }catch (err) {
            res.status(401).send(err); 
            throw err;
        }

       

    },

    getOrders : async (req, res) =>{
        
        try{
            sendToAirtableLogs('Pharmacy getting DB')
            let result = await Order.find({answered:false})
            res.status(200).send(result)
        }catch (err){
            res.status(401).send(err);
            throw err;
        }
       
    },

    getOrder : async (req, res) =>{
        
        try{
            let orderID = req.query.order
            if(!orderID){
                res.status(400).send({message:'missing id'})
                return
            }
            sendToAirtableLogs('Pharmacy responding', orderID, "", "", "")
            sendToSlack('Farmacia consultando: ' + orderID, SLACK_URL_DASHBOARD)

            let order = await Order.findById(orderID)
            res.status(200).send([order])
        }catch (err){
            res.status(401).send(err);
            throw err;
        }
       
    },

    respondOrder : async (req,res) =>{
        console.log(req.body.products)

        let userId = req.body.userId;
        let products = req.body.products;
        let _id = req.body._id

        sendToAirtableLogs('Pharmacy responding', _id, "", "", products)
        sendToSlack('Farmacia respondiendo: ' + orderID, SLACK_URL_DASHBOARD)

        switch(userId){
            case !'':
                result = await sendResponseToFB(userId, products, _id);
                break;
            case '':
                 result = await sendToTwilo(products, _id)
                break;

        }
        
        res.status(200).send({message:'ok'})
    },

    getReplayedOrder: async (req, res) =>{
        let orderID = req.query.order
        if(!orderID){
            res.status(400).send({message:'missing id'})
            return
        }
        sendToAirtableLogs('Cliente apunto de comprar', orderID, "", "", "")
        sendToSlack('Cliente consultando: ' + orderID, SLACK_URL_DASHBOARD)

        let order = await ReplayedOrder.findById(orderID)
        res.status(200).send(order)
    },

    buyOrder : async (req, res) =>{
        try{
            let order = await ReplayedOrder.findById(req.body._id)

            sendToAirtableLogs('Cliente compro', order.name, order.userPhone , order.zip, order.products)
            sendToSlack('VENTA \n \n Cliente : ' + order.name + '\n' + 'whatsapp : https://api.whatsapp.com/send?phone=+521' + order.userPhone + '\n \n' + 'productos: ' + order.products.join(''), SLACK_URL_DASHBOARD)

            let textToSend = 'Orden de compra:\n' + 'Cliente: ' + order.name + '\n' + 'Número: ' + order.userPhone + '\n' + '\n'+ 'Productos: '  + '\n';
            req.body.products.map(product => {
                if(product.price !== undefined && product.price !== '0'){
                    textToSend += product.product + ' - $' + product.price + ' num: ' + product.quantity + '\n';
                }
    
            })
            textToSend +=   '\n' + '\n' + 'Precio final: \n' + req.body.finalPrice
            console.log('slackUrl : ', order.slackUrl)
            sendToSlack(textToSend, order.slackUrl)
            res.status(200).send({message:'ok'})
        }
        catch(err){
            res.status(400).send({error:err})
        }
        
    }

}