var express = require('express');

const controllers = require('../controllers/customer');

var router = express.Router();

router.post('/sendDataToPharmacy', controllers.saveCustomerProductsAndSendToPharmacy)
router.get('/Orders', controllers.getOrders)
router.get('/Order', controllers.getOrder)
router.post('/respondOrder', controllers.respondOrder)
router.post('/buyOrder', controllers.buyOrder)
router.get('/replayedOrders', controllers.getReplayedOrder)



module.exports = router; 